package com.example.admin.mqtt;

import android.os.Bundle;

import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class AirActivity extends AppCompatActivity implements View.OnClickListener {
    private Button air_power;
    private Button autoMatic;
    private Button model;
    private Button temperature_up;
    private Button temperature_down;
    private Button wind_up;
    private Button wind_down;
    private Button wind_ud;
    private Button wind_lr;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_air_control);

        air_power=findViewById(R.id.air_power);
        autoMatic=findViewById(R.id.auto_matic);
        model=findViewById(R.id.model);
        temperature_up=findViewById(R.id.temperature_up);
        temperature_down=findViewById(R.id.temperature_down);
        wind_up=findViewById(R.id.wind_up);
        wind_down=findViewById(R.id.wind_down);
        wind_ud=findViewById(R.id.wind_ud);
        wind_lr=findViewById(R.id.wind_lr);
        air_power.setOnClickListener(this);
        autoMatic.setOnClickListener(this);
        model.setOnClickListener(this);
        temperature_up.setOnClickListener(this);
        temperature_down.setOnClickListener(this);
        wind_up.setOnClickListener(this);
        wind_down.setOnClickListener(this);
        wind_ud.setOnClickListener(this);
        wind_lr.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

        //获取组件的资源id
        int id = v.getId();
        switch (id) {
            case R.id.air_power:
                MainActivity.mqtt.publishmessageplus("air_control","{\"power\":1}");
                break;
            case R.id.auto_matic:
                MainActivity.mqtt.publishmessageplus("air_control","{\"autoMatic\":1}");
                break;
            case R.id.model:
                MainActivity.mqtt.publishmessageplus("air_control","{\"model\":1}");
                break;
            case R.id.temperature_up:
                MainActivity.mqtt.publishmessageplus("air_control","{\"temperature_up\":1}");
                break;
            case R.id.temperature_down:
                MainActivity.mqtt.publishmessageplus("air_control","{\"temperature_down\":1}");
                break;
            case R.id.wind_up:
                MainActivity.mqtt.publishmessageplus("air_control","{\"wind_up\":1}");
                break;
            case R.id.wind_down:
                MainActivity.mqtt.publishmessageplus("air_control","{\"wind_down\":1}");
                break;
            case R.id.wind_ud:
                MainActivity.mqtt.publishmessageplus("air_control","{\"wind_ud\":1}");
                break;
            case R.id.wind_lr:
                MainActivity.mqtt.publishmessageplus("air_control","{\"wind_lr\":1}");
                break;
            default:
                break;
        }
    }
}