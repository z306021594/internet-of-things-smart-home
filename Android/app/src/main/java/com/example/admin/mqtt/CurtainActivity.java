package com.example.admin.mqtt;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

//import android.support.v4.view.PagerAdapter;
//import android.support.v4.view.ViewPager;

public class CurtainActivity extends AppCompatActivity {
    private Button left;
    private Button right;
    private static ViewPager vp;
    private MyAdapter myAdapter;
    private static List<ImageView> imgList;
    private ImageView iv;
    // 获取到图片
    private Integer[] img = {R.drawable.pic1, R.drawable.pic2,
            R.drawable.pic3, R.drawable.pic4};
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_curtain);
        vp=findViewById(R.id.viewpager);
        imgList = new ArrayList<ImageView>();

        // hum_up = (Button) LayoutInflater.from(MainActivity.this).inflate(R.layout.fg1, null).findViewById(R.id.hum_up);
        iv=new ImageView(this);
        // 将图片放到集合中
        for (int i = 0; i < img.length; i++) {
            ImageView iv = new ImageView(this);
            iv.setImageResource(img[i]);
            // 将图片全屏显示
            iv.setScaleType(ImageView.ScaleType.FIT_XY);
            imgList.add(iv);
        }
        myAdapter = new MyAdapter();
        vp.setAdapter(myAdapter);
        left = findViewById(R.id.left_go);
        right = findViewById(R.id.right_go);
        left.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction()==MotionEvent.ACTION_DOWN) {
                    MainActivity.mqtt.publishmessageplus("curtain_control", "{\"power\":\"left\"}");

                }
                if(event.getAction()==MotionEvent.ACTION_UP) {
                    MainActivity.mqtt.publishmessageplus("curtain_control", "{\"power\":\"off\"}");
                }
                return false;
            }
        });
        right.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction()==MotionEvent.ACTION_DOWN) {
                    MainActivity.mqtt.publishmessageplus("curtain_control", "{\"power\":\"right\"}");

                }
                if(event.getAction()==MotionEvent.ACTION_UP)
                    MainActivity.mqtt.publishmessageplus("curtain_control","{\"power\":\"off\"}");
                return false;
            }
        });
    }
    /**
     * 自定义适配器
     *
     * @author Administrator
     */
    static class MyAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return imgList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            // TODO Auto-generated method stub
            return view == obj;
        }

        /**
         * 实例化视图内容（创建要显示的内容）
         */
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            // TODO Auto-generated method stub

            container.addView(imgList.get(position));
            return imgList.get(position);
        }

        /**
         * 销毁视图内容
         */
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            // TODO Auto-generated method stub
            container.removeView((View) object);
        }

    }
}
