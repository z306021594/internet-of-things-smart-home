package com.example.admin.mqtt;

import android.annotation.SuppressLint;
import android.app.Service;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;

import static com.example.admin.mqtt.MainActivity.getMessage;
import static com.example.admin.mqtt.MainActivity.mqtt;

public class DoorActivity extends AppCompatActivity implements View.OnClickListener {
    private Button finger_add;
    private Button finger_delete;
    private Button finger_get;
    private Button finger_work;
    private static SimpleAdapter simpleAdapter;
    Vibrator vibrator;
    private ListView item;
    private static List<Map<String, Object>> dataList;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_door_control);
        initView();
        dataList = new ArrayList<Map<String, Object>>();
        Map<String, Object> map = new HashMap<String, Object>();
        for(int i=1;i<=1;i++) {
            map.put("ig1", R.drawable.list_back);
            map.put("tv1", "用户");
            map.put("tv2", "用户ID: ");
            dataList.add(map);
        }
        vibrator = (Vibrator) getSystemService(Service.VIBRATOR_SERVICE);
        simpleAdapter = new SimpleAdapter(DoorActivity.this, dataList, R.layout.list_item, new String[]{"ig1", "tv1", "tv2"}, new int[]{R.id.ig1, R.id.tv1, R.id.tv2});
        item.setAdapter(simpleAdapter); //绑适配器
        mqtt.door_handler=new Handler(){
            @SuppressLint("SetTextI18n")
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case 1: //开机校验更新回传
                        try {
                            String str_door=null;
                            JSONObject jsonObject = null;
                            jsonObject = new JSONObject(msg.obj.toString());
                            if (jsonObject.getString("door_msg") != null)
                                str_door=jsonObject.getString("door_msg");
                            if(msg.obj.toString().contains("ok"))
                                Toast.makeText(DoorActivity.this, str_door, Toast.LENGTH_SHORT).show();
                            else if(msg.obj.toString().contains("用户")) {

                                Toast.makeText(DoorActivity.this, "操作成功", Toast.LENGTH_SHORT).show();
                                str_door = str_door.replaceAll("\r|\n", "");
                                String[] res = str_door.split(",");
                                for(String i:res){
                                    i.replace("\r\n"," ");
                                    System.out.println("get"+i);
                                }
                                int i = 0;
                                dataList.clear();
                                for(String res_index:res){
                                    Map<String, Object> map3 = new HashMap<String, Object>();
                                    map3.put("ig1", R.drawable.list_back);
                                    map3.put("tv1", "用户:" + i);
                                    map3.put("tv2", "用户ID:" + "     " + res_index);
                                    dataList.add(map3);
                                    i++;
                                }
                                simpleAdapter.notifyDataSetChanged();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();


                        }
                        break;
                        default:
                            break;
                }
            }
        };

    }

    private void initView() {
        finger_add = (Button) findViewById(R.id.finger_add);
        finger_delete = (Button) findViewById(R.id.finger_delete);
        finger_get = (Button) findViewById(R.id.finger_get);
        finger_work = (Button) findViewById(R.id.finger_work);
        item = (ListView) findViewById(R.id.item);

        finger_add.setOnClickListener(this);
        finger_delete.setOnClickListener(this);
        finger_get.setOnClickListener(this);
        finger_work.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.finger_add:
                mqtt.publishmessageplus("door_control","{\"door_cmd\":\"add\"}");
                break;
            case R.id.finger_delete:
                mqtt.publishmessageplus("door_control","{\"door_cmd\":\"delete\"}");
                break;
            case R.id.finger_get:
                mqtt.publishmessageplus("door_control","{\"door_cmd\":\"list\"}");
                break;
            case R.id.finger_work:
                mqtt.publishmessageplus("door_control","{\"door_cmd\":\"work\"}");
                break;
        }
    }

}
