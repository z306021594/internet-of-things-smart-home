package com.example.admin.mqtt;

public class GetMessage {
    private String temperture;
    private String light;
    private String hum;

    public String getDoor_msg() {
        return door_msg;
    }

    public void setDoor_msg(String door_msg) {
        this.door_msg = door_msg;
    }

    private String door_msg;
    public String getTemperture() {
        return temperture;
    }

    public void setTemperture(String temperture) {
        this.temperture = temperture;
    }

    public String getLight() {
        return light;
    }

    public void setLight(String light) {
        this.light = light;
    }

    public String getHum() {
        return hum;
    }

    public void setHum(String hum) {
        this.hum = hum;
    }
}
