package com.example.admin.mqtt;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import static com.example.admin.mqtt.R.drawable.led_bed1;

public class LedActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView led_bed1;
    private ImageView led_bed2;
    private ImageView led_bath;
    private ImageView led_kitchen;
    private ImageView led_sitting;
    private ImageView led_room;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_light);
        initView();
        led_bed1.setOnClickListener(this);
        led_bed2.setOnClickListener(this);
        led_bath.setOnClickListener(this);
        led_kitchen.setOnClickListener(this);
        led_sitting.setOnClickListener(this);
        led_room.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.led_bed1:

                if(led_bed1.getBackground().getCurrent().getConstantState().equals(getResources().getDrawable(R.drawable.led_bed1).getConstantState())){
                    Log.v("led_bed","open");
                    MainActivity.mqtt.publishmessageplus("led_control","{\"led2\":\"open\"}");
                    led_bed1.setBackgroundResource(R.drawable.led_bed1_black);
                }else if(led_bed1.getBackground().getCurrent().getConstantState().equals(getResources().getDrawable(R.drawable.led_bed1_black).getConstantState())){
                    Log.v("led_bed","close");
                    MainActivity.mqtt.publishmessageplus("led_control","{\"led2\":\"close\"}");
                    led_bed1.setBackgroundResource(R.drawable.led_bed1);

                }

                break;
            case R.id.led_bed2:

                if(led_bed2.getBackground().getCurrent().getConstantState().equals(getResources().getDrawable(R.drawable.led_bed2).getConstantState())) {
                    MainActivity.mqtt.publishmessageplus("led_control","{\"led2\":\"open\"}");
                    led_bed2.setBackgroundResource(R.drawable.led_bed2_black);
                }else{
                    MainActivity.mqtt.publishmessageplus("led_control","{\"led2\":\"close\"}");
                    led_bed2.setBackgroundResource(R.drawable.led_bed2);
                }
                break;
            case R.id.led_bath:

                if(led_bath.getBackground().getCurrent().getConstantState().equals(getResources().getDrawable(R.drawable.led_bath).getConstantState())) {
                    MainActivity.mqtt.publishmessageplus("led_control","{\"led2\":\"open\"}");
                    led_bath.setBackgroundResource(R.drawable.led_bath_black);
                }else{
                    MainActivity.mqtt.publishmessageplus("led_control","{\"led2\":\"close\"}");
                    led_bath.setBackgroundResource(R.drawable.led_bath);
                }
                break;
            case R.id.led_room:
                if(led_room.getBackground().getCurrent().getConstantState().equals(getResources().getDrawable(R.drawable.led_room).getConstantState())) {
                    MainActivity.mqtt.publishmessageplus("led_control","{\"led2\":\"open\"}");
                    led_room.setBackgroundResource(R.drawable.led_room_black);
                }else{
                    MainActivity.mqtt.publishmessageplus("led_control","{\"led2\":\"close\"}");
                    led_room.setBackgroundResource(R.drawable.led_room);
                }
                break;
            case R.id.led_kitchen:
                if(led_kitchen.getBackground().getCurrent().getConstantState().equals(getResources().getDrawable(R.drawable.led_kitchen).getConstantState())) {
                    MainActivity.mqtt.publishmessageplus("led_control","{\"led2\":\"open\"}");
                    led_kitchen.setBackgroundResource(R.drawable.led_kitchen_black);
                }else{
                    MainActivity.mqtt.publishmessageplus("led_control","{\"led2\":\"close\"}");
                    led_kitchen.setBackgroundResource(R.drawable.led_kitchen);
                }
                break;
            case R.id.led_sitting:
                if(led_sitting.getBackground().getCurrent().getConstantState().equals(getResources().getDrawable(R.drawable.led_sitting).getConstantState())) {
                    MainActivity.mqtt.publishmessageplus("led_control","{\"led2\":\"open\"}");
                    led_sitting.setBackgroundResource(R.drawable.led_sitting_black);
                }else{
                    MainActivity.mqtt.publishmessageplus("led_control","{\"led2\":\"close\"}");
                    led_sitting.setBackgroundResource(R.drawable.led_sitting);
                }
                break;
        }

    }

    private void initView() {
        led_bed1 = (ImageView) findViewById(R.id.led_bed1);
        led_bed2 = (ImageView) findViewById(R.id.led_bed2);
        led_bath = (ImageView) findViewById(R.id.led_bath);
        led_kitchen = (ImageView) findViewById(R.id.led_kitchen);
        led_sitting = (ImageView) findViewById(R.id.led_sitting);
        led_room = (ImageView) findViewById(R.id.led_room);
    }
}
