#include <WiFi.h>
// #include <U8g2lib.h>
//#include <I2Cdev.h>
#include <Wire.h>
// #include <MPU6050.h>
#include "Pins.h"
#include "Camera.h"
#include "Motor.h"

const char* ssid = "Fishing WiFi ";
const char* password = "zheshiyigewifi";

WiFiServer server(81);

// OV2640 camera
Camera ov2640;

void setup()
{
  Serial.begin(115200);
  ov2640.initialize();
  int n = WiFi.scanNetworks();
  Serial.println("scan done");
  if (n == 0)
  {
    Serial.println("no networks found");
  }
  else
  {
    Serial.print(n);
    Serial.println(" networks found");
    for (int i = 0; i < n; ++i)
    {
      // Print SSID and RSSI for each network found
      Serial.print(i + 1);
      Serial.print(": ");
      Serial.print(WiFi.SSID(i));
      Serial.print(" (");
      Serial.print(WiFi.RSSI(i));
      Serial.print(")");
      Serial.println((WiFi.encryptionType(i) == WIFI_AUTH_OPEN) ? " " : "*");
      delay(10);
    }
  }

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");

  ov2640.startCameraServer();

  Serial.print("Camera Ready! Use 'http://");
  Serial.print(WiFi.localIP());
  Serial.println("' to connect");

  server.begin();
}


float i = -1;
long heart_beat = 0;
void loop()
{

  Serial.println(analogRead(38));

  WiFiClient client = server.available();   // listen for incoming clients
  if (client)
  {
    // if you get a client,
    Serial.println("New Client.");           // print a message out the serial port
    String currentLine = "";                // make a String to hold incoming data from the client
    while (client.connected())
    {            // loop while the client's connected
      if (client.available())
      {             // if there's bytes to read from the client,
    
      }
    }
  }

  delay(50);
}
String getValue(String data, char separator, int index)
{
  int found = 0;
  int strIndex[] = { 0, -1 };
  int maxIndex = data.length() - 1;

  for (int i = 0; i <= maxIndex && found <= index; i++)
  {
    if (data.charAt(i) == separator || i == maxIndex)
    {
      found++;
      strIndex[0] = strIndex[1] + 1;
      strIndex[1] = (i == maxIndex) ? i + 1 : i;
    }
  }
  return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}
